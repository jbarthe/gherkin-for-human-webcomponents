;(function (exports, document, Object, HTMLElement) {
  'use strict';

  exports.GherkinEditor = {};

  var GherkinEditorPrototype = Object.create(HTMLElement.prototype);

  GherkinEditorPrototype.createdCallback = function() {
    var feature = new exports.GherkinEditor.Feature();
    this.appendChild(feature);
  };

  document.registerElement('gherkin-editor', {
    prototype: GherkinEditorPrototype
  });

})(
    window,
    window.document,
    window.Object,
    window.HTMLElement
);
