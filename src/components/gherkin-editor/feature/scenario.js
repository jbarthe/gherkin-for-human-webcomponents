;(function (exports, document, Object, HTMLElement) {
  'use strict';

  var ScenarioPrototype = Object.create(HTMLElement.prototype);

  ScenarioPrototype.createdCallback = function() {
    this.className = 'scenario-container';

    var title = new exports.Scenario.Title();
    this.appendChild(title);
  };

  var Scenario = document.registerElement('scenario-container', {
    prototype: ScenarioPrototype,
    extends: 'section'
  });

  exports.Scenario = Scenario;

})(
    window.GherkinEditor.Feature,
    window.document,
    window.Object,
    window.HTMLElement
);
