;(function (exports, document, Object, HTMLElement, ContentEditable) {
  'use strict';

  var ScenarioTitlePrototype = Object.create(HTMLElement.prototype);

  ScenarioTitlePrototype.createdCallback = function() {
    this.textContent = "Scenario: ";

    var content = new ContentEditable();
    this.appendChild(content);
  };

  var ScenarioTitle = document.registerElement('scenario-title', {
    prototype: ScenarioTitlePrototype,
    extends: 'h2'
  });

  exports.Title = ScenarioTitle;

})(
    window.GherkinEditor.Feature.Scenario,
    window.document,
    window.Object,
    window.HTMLElement,
    window.GherkinEditor.ContentEditable
);
