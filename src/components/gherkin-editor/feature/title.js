;(function (exports, document, Object, HTMLElement, ContentEditable) {
  'use strict';

  var FeatureTitlePrototype = Object.create(HTMLElement.prototype);

  FeatureTitlePrototype.createdCallback = function() {
    this.textContent = "Feature: ";

    var content = new ContentEditable();
    this.appendChild(content);
  };

  var FeatureTitle = document.registerElement('feature-title', {
    prototype: FeatureTitlePrototype,
    extends: 'h1'
  });

  exports.Title = FeatureTitle;

})(
    window.GherkinEditor.Feature,
    window.document,
    window.Object,
    window.HTMLElement,
    window.GherkinEditor.ContentEditable
);
