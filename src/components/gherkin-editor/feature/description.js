;(function (exports, document, Object, HTMLElement) {
  'use strict';

  var FeatureDescriptionPrototype = Object.create(HTMLElement.prototype);

  FeatureDescriptionPrototype.createdCallback = function() {
    this.textContent = "As a user I can";
    this.setAttribute('contenteditable', true);
    this.setAttribute('class', 'content-editable');
  };

  var FeatureDescription = document.registerElement('feature-description', {
    prototype: FeatureDescriptionPrototype,
    extends: 'div'
  });

  exports.Description = FeatureDescription;

})(
    window.GherkinEditor.Feature,
    window.document,
    window.Object,
    window.HTMLElement
);
