;(function (exports, document, Object, HTMLElement) {
  'use strict';

  var FeatureToolbarPrototype = Object.create(HTMLElement.prototype);

  FeatureToolbarPrototype.constructor = function (section) {
    this.section = section;
  };

  FeatureToolbarPrototype.createdCallback = function() {
    this.className = 'feature-toolbar';

    var addScenario = document.createElement('button');
    addScenario.textContent = 'Add a new scenario';

    addScenario.addEventListener('click', this.addScenario.bind(this));

    this.appendChild(addScenario);
  };

  FeatureToolbarPrototype.addScenario = function() {
    var scenario = new exports.Scenario();
    this.parentNode.appendChild(scenario);
  };

  var FeatureToolbar = document.registerElement('feature-toolbar', {
    prototype: FeatureToolbarPrototype,
    extends: 'div'
  });

  exports.Toolbar = FeatureToolbar;

})(
    window.GherkinEditor.Feature,
    window.document,
    window.Object,
    window.HTMLElement
);
