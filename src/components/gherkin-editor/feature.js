;(function (exports, document, Object, HTMLElement) {
  'use strict';

  var FeaturePrototype = Object.create(HTMLElement.prototype);

  FeaturePrototype.createdCallback = function() {
    var title = new exports.Feature.Title();

    var description = new exports.Feature.Description();

    var section = document.createElement('section');
    section.className = 'scenario-list';

    var toolbar = new exports.Feature.Toolbar();

    var firstScenario = new exports.Feature.Scenario();

    this.appendChild(title);
    this.appendChild(description);
    this.appendChild(toolbar);
    this.appendChild(section);
    section.appendChild(firstScenario);
  };

  var Feature = document.registerElement('feature-container', {
    prototype: FeaturePrototype,
    extends: 'section'
  });

  exports.Feature = Feature;

})(
    window.GherkinEditor,
    window.document,
    window.Object,
    window.HTMLElement
);
