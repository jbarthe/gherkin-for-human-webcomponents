;(function (exports, document, Object, HTMLElement) {
  'use strict';

  var ContentEditablePrototype = Object.create(HTMLElement.prototype);

  ContentEditablePrototype.createdCallback = function() {
    this.textContent = ' ';
    this.setAttribute('contenteditable', true);
    this.setAttribute('class', 'content-editable');
  };

  var ContentEditable = document.registerElement('content-editable', {
    prototype: ContentEditablePrototype,
    extends: 'span'
  });

  exports.ContentEditable = ContentEditable;

})(
    window.GherkinEditor,
    window.document,
    window.Object,
    window.HTMLElement
);
